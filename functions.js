
// Create a function "correctName" that displays properly capitalized name
// If "joe" is input the output will be "joe"

//Pseudocode:
// 1. Convert string to lowercase "jOE" -> "joe"
// 2. Extract first character "joe" -> "j"
// 3. Convert first character to uppercase "j" -> "J"
// 4. Extract all characters but first one "joe" -> "oe"
// 5. Combine first (now uppercase) character with the lowercase characters "J" + "oe"
// 6. Return the result

function correctName(strName)
{
    // 1. Convert string to lowercase "jOE" -> "joe"
    var allLower = strName.toLowerCase();
    // 2. Extract first character "joe" -> "j"
    var firstChar = allLower.charAt(0);
    // 3. Convert first character to uppercase "j" -> "J"
    firstChar = firstChar.toUpperCase();
    // 4. Extract all characters but first one "joe" -> "oe"
    var smallRest = allLower.substring(1, strName.length);
    // 5. Combine first (now uppercase) character with the lowercase characters "J" + "oe"
    var strResult = firstChar + smallRest;
    // 6. Return the result
    return strResult;
}

console.log(correctName("JoHnny"));