class Dog {
    constructor(name, age, breed, price) {
        this.name = name;
        this.age = age;
        this.breed = breed;
        this.price = price;
    }

    // show dog's price within $
    showPrice() {
        return "$" + this.price;
    }

    // display info about the doggo
    toString() {
        let strInfo = "";
        strInfo = strInfo + "Name: " + this.name + "\n";
        strInfo += "Age: " + this.age + "\n";
        strInfo += "Breed: " + this.breed + "\n";
        strInfo += "Price: " + this.price + "\n";

        return strInfo;
    }
}

// let us create a few dogs

let rex = new Dog("Rex", 3, "German Shepherd", 2000);
let bonnie = new Dog("Bonnie", 2, "Poodle", 1000);
let hank = new Dog("Hank", 2, "Labrador", 1200);
let tom = new Dog("Tom", 1, "Great Dane", 2500);

// show Rex and Tom
// console.log(rex.toString());
// console.log(tom.toString());

// // show prices for Rex and Tom
// console.log(rex.showPrice());
// console.log(tom.showPrice());

// create an array from all doggos
var dogs = [];
dogs.push(rex);
dogs.push(tom);
dogs.push(hank);
dogs.push(bonnie);

// display number of dogs
//console.log("\n" + dogs.length);

// write a loop displaying all dogs
for (let index = 0; index < dogs.length; index++) {
    console.log(dogs[index].toString());
}

// create an HTML table containing all dogs
/*
<table border="1">
  <tr>
    <td>Rex</td>
    <td>3</td>
    <td>German Shephard</td>
    <td>$2000</td>
  </tr>
  <tr>
    <td>Tom</td>
    <td>1</td>
    <td>Great Dane</td>
    <td>$2500</td>
  </tr>
  <tr>
    <td>Hank</td>
    <td>2</td>
    <td>Labrador</td>
    <td>$1200</td>
  </tr>
  <tr>
    <td>Bonnie</td>
    <td>2</td>
    <td>Poodle</td>
    <td>$1000</td>
  </tr>
</table>
*/
function createDogsHTMLTable() {
    var strTable;
    strTable = '<table border="1">';
    for (var index = 0; index < dogs.length; index++) {
        strTable = strTable + "<tr>";

        strTable += "<td>" + dogs[index].name + "</td>";
        strTable += "<td>" + dogs[index].age + "</td>";
        strTable += "<td>" + dogs[index].breed + "</td>";
        strTable += "<td>" + dogs[index].price + "</td>";

        strTable = strTable + "</tr>";
    }

    strTable = strTable + '</table>';
    return strTable;
}
// console.log(strTable);
document.getElementById('mytable').innerHTML = createDogsHTMLTable(dogs);


var jim = new Dog("Jim", 22, "Human", 3333);
var joe = new Dog("Joe", 25, "Lion", 3);
var puppies = [];
puppies.push(jim);
puppies.push(joe);